package com.hencoder.hencoderpracticedraw1.practice;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;

public class Practice10HistogramView extends View {

    public Practice10HistogramView(Context context) {
        super(context);
    }

    public Practice10HistogramView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public Practice10HistogramView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    Paint mPaint = new Paint();
    Paint paintTxt = new Paint();
    Paint painM = new Paint();
    Paint paintRect = new Paint();
    RectF mRectF1 = new RectF(220f,695f,320f,700f);
    RectF mRectF2 = new RectF(350f,675f,450f,700f);
    RectF mRectF3 = new RectF(490f,670f,590f,700f);
    RectF mRectF4 = new RectF(630f,605f,730f,700f);
    RectF mRectF5 = new RectF(790f,505f,890f,700f);
    RectF mRectF6 = new RectF(910f,475f,1010f,700f);
    RectF mRectF7 = new RectF(1050f,64f,1150f,700f);





    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

//        综合练习
//        练习内容：使用各种 Canvas.drawXXX() 方法画直方图

        paintTxt.setTextSize(35f);
        paintTxt.setColor(Color.WHITE);
        mPaint.setColor(Color.WHITE);
        paintRect.setColor(Color.GREEN);
        painM.setTextSize(60f);
        painM.setColor(Color.WHITE);
        canvas.drawLine(200f,100f,200f,700f,mPaint);
        canvas.drawLine(200f,700f,1200f,700f,mPaint);
        canvas.drawText("Froyo",230f,735f,paintTxt);
        canvas.drawText("GB",372,735f,paintTxt);
        canvas.drawText("IC S",514f,735f,paintTxt);
        canvas.drawText("JB",656f,735f,paintTxt);
        canvas.drawText("KiKat",798f,735f,paintTxt);
        canvas.drawText("L",940f,735f,paintTxt);
        canvas.drawText("M",1082f,735f,paintTxt);
        canvas.drawText("直方图", 550f,900f,painM);

        canvas.drawRect(mRectF1,paintTxt);
        canvas.drawRect(mRectF2,paintRect);
        canvas.drawRect(mRectF3,paintRect);
        canvas.drawRect(mRectF4,paintRect);
        canvas.drawRect(mRectF5,paintRect);
        canvas.drawRect(mRectF6,paintRect);
        canvas.drawRect(mRectF7,paintRect);

    }
}
