package com.hencoder.hencoderpracticedraw1.practice;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;

public class Practice11PieChartView extends View {

  public Practice11PieChartView(Context context) {
    super(context);
  }

  public Practice11PieChartView(Context context, @Nullable AttributeSet attrs) {
    super(context, attrs);
  }

  public Practice11PieChartView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
  }

  Paint paint1 = new Paint();
  Paint paint2 = new Paint();
  Paint paint3 = new Paint();
  Paint paint4 = new Paint();
  Paint paint5 = new Paint();
  Paint paint6 = new Paint();
  Paint paint7 = new Paint();

  Paint paintT = new Paint();
  Paint paintL = new Paint();
  RectF rectf1 = new RectF(300f, 200f, 920f, 820f);
  RectF rectf2 = new RectF(330f, 230f, 930f, 830f);

  float[] float1 = new float[]{250f,200f,280f,250f};
  @Override protected void onDraw(Canvas canvas) {
    super.onDraw(canvas);

    //        综合练习
    //        练习内容：使用各种 Canvas.drawXXX() 方法画饼图

    paint1.setColor(Color.RED);
    paint2.setColor(Color.YELLOW);
    paint3.setColor(Color.TRANSPARENT);
    paint4.setColor(Color.RED|Color.GRAY);
    paint5.setColor(Color.GRAY);
    paint6.setColor(Color.GREEN);
    paint7.setColor(Color.BLUE);

    paintT.setTextSize(35f);
    paintT.setColor(Color.WHITE);
    paintL.setStrokeWidth(4f);
    paintL.setColor(Color.WHITE);
    canvas.drawText("Lollipop",200f,200f,paintT);
    canvas.drawLine(360f,190f,460f,190f,paintL);
    canvas.drawLine(460f,190f,560f,250f,paintL);


    canvas.drawArc(rectf1,-180f,135f,true,paint1);
    canvas.drawArc(rectf2,-45f, 45f,true,paint2);
    canvas.drawArc(rectf2,0f, 3f,true,paint3);
    canvas.drawArc(rectf2,3f, 10f,true,paint4);
    canvas.drawArc(rectf2,15f, 10f,true,paint5);
    canvas.drawArc(rectf2,27f, 50f,true,paint6);
    canvas.drawArc(rectf2,79f, 101f,true,paint7);



  }
}
