package com.hencoder.hencoderpracticedraw1.practice;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;

public class Practice2DrawCircleView extends View {

    public Practice2DrawCircleView(Context context) {
        super(context);
    }

    public Practice2DrawCircleView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public Practice2DrawCircleView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    Paint paint1 = new Paint();
    Paint paint2 = new Paint();
    Paint paint3 = new Paint();
    Paint paint4 = new Paint();

    Path mPath = new Path();
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

//        练习内容：使用 canvas.drawCircle() 方法画圆
//        一共四个圆：1.实心圆 2.空心圆 3.蓝色实心圆 4.线宽为 20 的空心圆


        canvas.drawCircle(300f,250f,200f,paint1);

        paint2.setStyle(Paint.Style.STROKE);
        canvas.drawCircle(800f,250f, 200f,paint2);

        paint3.setColor(Color.BLUE);
        canvas.drawCircle(300f, 700f, 200f, paint3);

        paint4.setStyle(Paint.Style.STROKE);
        paint4.setStrokeWidth(50f);
        canvas.drawCircle(800f, 700f, 200f, paint4);

    }
}
