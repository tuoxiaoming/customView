package com.hencoder.hencoderpracticedraw1.practice;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;

public class Practice9DrawPathView extends View {

    public Practice9DrawPathView(Context context) {
        super(context);
    }

    public Practice9DrawPathView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public Practice9DrawPathView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }


    Paint mPaint =  new Paint();
    Path mPath = new Path();
    RectF mRectF = new RectF(300f,300f,700f,700f);
    RectF mRectF1 = new RectF(700f,300f,1100f,700f);
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

//        练习内容：使用 canvas.drawPath() 方法画心形


        mPath.addArc(mRectF,-225f,225f);
        mPath.arcTo(mRectF1,-180f,225f,true);
        mPath.lineTo(700f,900f);
        mPath.lineTo(359.2895f,640.7105f);
        canvas.drawPath(mPath,mPaint);
    }
}
